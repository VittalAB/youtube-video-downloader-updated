from flask import *
from pytube import YouTube as yt
import os
from datetime import datetime
from flask import current_app
import shutil
import urllib.request
import json
import urllib
from urllib.parse import *
import string
import random
import pprint
from flask import request
from utils import YTD as ytd


app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/submit_audio', methods=['post', 'get'])
def submit_audio():
    data = request.form.get('link')

    y_obj = ytd(data, 'audio')

    if y_obj.is_age_restricted():
        return render_template('error.html')

    write_path, data = y_obj.download_audio()

    file_size = y_obj.get_youtube_video_size(write_path)

    if data:
        del data['html']
        del data['height']
        del data['width']
        del data['thumbnail_height']
        del data['thumbnail_width']

        return render_template('result.html', audio_name=write_path, data=data, file_size=file_size)
    else:
        return render_template('result.html', audio_name=write_path, file_size=file_size)


@app.route('/submit', methods=['POST', 'GET'])
def submit():

    data = request.form.get('link')

    select = request.form.get('select')

    y_obj = ytd(data, 'video')

    if y_obj.is_age_restricted():
        return render_template('error.html')

    write_path, data = y_obj.download_video(select)

    file_size = y_obj.get_youtube_video_size(write_path)

    if not data:
        return render_template('result.html', video_name=write_path, file_size=file_size)
    else:
        del data['html']
        del data['height']
        del data['width']
        del data['thumbnail_height']
        del data['thumbnail_width']

        return render_template('result.html', video_name=write_path, data=data, file_size=file_size)


@app.route('/download/<path:filename>', methods=['GET'])
def download(filename):
    directory = 'videos'  # Replace with the actual directory where video files are stored
    return send_from_directory(directory, filename, as_attachment=True, download_name=filename)


@app.route('/download_audio/<path:filename>', methods=['GET'])
def download_audio_file(filename):
    directory = 'audios'  # Replace with the actual directory where audio files are stored
    return send_from_directory(directory, filename, as_attachment=True, download_name=filename)


if __name__ == "__main__":
    app.run(debug=False, port=5555, host="0.0.0.0")
