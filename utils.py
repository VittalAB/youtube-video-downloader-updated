from pytube import YouTube as yt
import string
import random
import shutil
import urllib.request
import json
import urllib
from urllib.parse import *
import os
from datetime import datetime
import youtube_dl

class YTD:

    def __init__(self, link, media_type):
        self.link = link
        self.media_type = media_type

    def randomword(self, length):
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(length))

    def get_video_id(self):

        query = urlparse(self.link)

        if query.hostname == 'youtu.be':
            return query.path[1:]

        if query.hostname in ('www.youtube.com', 'youtube.com'):

            if query.path == '/watch':
                p = parse_qs(query.query)
                return p['v'][0]

            if query.path[:7] == '/embed/':
                return query.path.split('/')[2]

            if query.path[:3] == '/v/':
                return query.path.split('/')[2]
        # fail?
        return None

    def get_video_title(self):

        if not self.get_video_id():
            VideoID = self.link[17:]
        else:
            VideoID = self.get_video_id()

        params = {"format": "json",
                  "url": "https://www.youtube.com/watch?v=%s" % VideoID}

        url = "https://www.youtube.com/oembed"

        query_string = urllib.parse.urlencode(params)
        url = url + "?" + query_string

        with urllib.request.urlopen(url) as response:
            response_text = response.read()
            data = json.loads(response_text.decode())
            name = data['title']

        f_name = ""
        for c in name:
            if 'a' <= c <= 'z' or 'A' <= c <= 'Z':
                f_name = f_name + c

        return f_name, data

    def download_audio(self):

        try:
            f_name, data = self.get_video_title()
        except:
            f_name = self.randomword(10)
            data = None

        if len(os.listdir('audios')) > 5 or f'{f_name}_.mp4' in os.listdir('audios'):
            shutil.rmtree('audios')
            os.mkdir('audios')

        url = yt(str(self.link))

        video = url.streams.filter(only_audio=True).first()

        out_file = video.download(output_path="audios",
                                  filename=str(f_name)+'_.mp3')
        os.system(
            f'ffmpeg -loop 1 -r 1 -i static/images/flyer.jpg -i audios/{f_name}_.mp3 -c:a copy -shortest -c:v libx264 audios/{f_name}_.mp4')

        os.remove(out_file)
        ret_file = f'audios/{f_name}_.mp4'

        with open("history.txt", "a") as myfile:
            myfile.write(
                "\n" + f"{datetime.now().strftime('%d/%m/%y__%H:%M:%S')} --> {self.link}  --> {f_name}" + "\n")
        return ret_file[7:], data

    def download_video(self, select):

        if len(os.listdir('videos')) > 2:
            shutil.rmtree('videos')
            os.mkdir('videos')

        url = yt(str(self.link))

        try:
            f_name, data = self.get_video_title()
        except:
            f_name = self.randomword(10)
            data = None

        if select == 'High':
            video = url.streams.get_highest_resolution()
        elif select == 'Low':
            video = url.streams.first()
        else:
            video = url.streams.get_lowest_resolution()

        out_file = video.download(output_path='videos',
                                  filename=str(f_name)+'_.mp4')
        ret_file = f'videos/{f_name}_.mp4'

        with open("history.txt", "a") as myfile:
            myfile.write(
                "\n" + f"{datetime.now().strftime('%d/%m/%y__%H:%M:%S')} --> {self.link}   --> {f_name}" + "\n")

        return ret_file[7:], data

    def get_youtube_video_size(self, location):
        # you can use below logic to put restriction on file size
        
        # try:
        #     video = yt(self.link)
        #     size_bytes = video.streams.get_lowest_resolution().filesize
        #     size_mb = size_bytes / (1024 * 1024)
        #     return round(size_mb)
        # except Exception as e:
        #     print("Error:", str(e))
        #     return 0

        if self.media_type == 'audio':
            file_size = (os.path.getsize('audios/' + location))//10**6
            return file_size
        else:
            file_size = (os.path.getsize('videos/' + location))//10**6
            return file_size

    def is_age_restricted(self):
        ydl_opts = {
            'quiet': True,
            'no_warnings': True,
            'skip_download': True,
            'extract_flat': True,
        }
        
        try:
            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                info = ydl.extract_info(self.link, download=False)
                return info.get('age_limit') is not None
        except Exception as e:
            print("Error:", e)

